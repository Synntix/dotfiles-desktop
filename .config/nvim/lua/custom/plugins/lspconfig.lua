local M = {}


M.setup_lsp = function(attach, capabilities)
    local lspconfig = require "lspconfig"

    lspconfig.rust_analyzer.setup {
        on_attach = attach,
        capabilities = capabilities,
    }

    lspconfig.clangd.setup {
        on_attach = attach,
        capabilities = capabilities,
    }
end

return M
