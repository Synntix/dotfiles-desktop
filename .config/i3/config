#    _ _____                    __ _       
#   (_)___ /    ___ ___  _ __  / _(_) __ _ 
#   | | |_ \   / __/ _ \| '_ \| |_| |/ _` |
#   | |___) | | (_| (_) | | | |  _| | (_| |
#   |_|____/   \___\___/|_| |_|_| |_|\__, |
#                                    |___/ 

# -------------------------------------------
#Mod
set $mod Mod1
set $sup Mod4

#Monitors
set $monitor_left DP-0
set $monitor_mid DP-2
set $monitor_right HDMI-0

#Assign workspaces to monitors
workspace 1 output $monitor_left
workspace 2 output $monitor_mid
workspace 3 output $monitor_right
workspace 4 output $monitor_left
workspace 5 output $monitor_mid
workspace 6 output $monitor_right

#Assign programs on particular workspaces
assign [class="^discord$"] 3


################ GENERAL CONFIG ################ 

# Font for window titles.
font pango:monospace 8

# You can also use any non-zero value if you'd like to have a border
for_window [class=".*"] border pixel 0

# Only enable gaps on a workspace when there is at least one container
smart_gaps on

#Gaps settings
gaps inner 15

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod


################## Personal things to launch at startup ########################

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork

# To get the graphical authentication popup
exec --no-startup-id /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
#Polybar
exec --no-startup-id /home/synntix/.config/polybar/launch.sh
#Discord
exec --no-startup-id discord
#Wallpaper
exec --no-startup-id feh --bg-scale /usr/share/backgrounds/archlinux/awesome.png
#exec --no-startup-id dyn-wall-rs -d ~/Images/dyn-wall
#Picom compositor
exec --no-startup-id picom --experimental-backends
#Driver for Razer devices
exec --no-startup-id openrazer-daemon
#Server for moc (headless audio player for soundboard)
exec --no-startup-id mocp -S


################ KEYBINDS ################ 

# kill focused window
bindsym $mod+Shift+a kill

# start a terminal
bindsym $mod+Return exec urxvt

# start rofi (a program launcher)
bindsym $mod+d exec --no-startup-id rofi -show drun -show-icons

# Use pactl to adjust volume in PulseAudio.
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5%
bindcode 193 exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5%
bindcode 194 exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle

# Media player controls
bindsym XF86AudioPlayPause exec --no-startup-id playerctl play-pause
bindsym XF86AudioPlay exec --no-startup-id playerctl play-pause
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous

#Custom bindings
bindcode $sup+49 exec urxvt -e vim -p ~/.config/i3/config ~/.config/picom/picom.conf ~/.config/polybar/config ~/.config/rofi/config.rasi
#bindcode $sup+49 exec atom ~/.config/i3/config ~/.config/picom/picom.conf ~/.config/polybar/config.ini ~/.config/rofi/config.rasi
bindsym $mod+o exec --no-startup-id notify-send "Select window to make opaque" && picom-trans -s 100
bindsym $sup+j exec bash -c "printf '♥' | iconv -f UTF8 -t UTF16 | xvkbd -utf16 -delay 50 -file -"
bindsym $sup+space exec --no-startup-id /home/synntix/.local/bin/switch-kb-lang
bindsym Print exec flameshot full --path /home/synntix/Images/
bindsym Ctrl+Print exec flameshot gui
bindsym $sup+f exec firefox
bindcode 191 exec firefox
bindsym $sup+a exec atom
bindsym $sup+n exec nemo
bindsym $sup+t exec --no-startup-id picom-trans 99

#Soundboard
bindsym $sup+c exec --no-startup-id /home/synntix/.local/bin/bahouiconnard
bindsym $sup+v exec --no-startup-id /home/synntix/.local/bin/triplebonk
bindsym $sup+b exec --no-startup-id /home/synntix/.local/bin/bonk
bindsym $sup+w exec --no-startup-id /home/synntix/.local/bin/wow
bindsym $sup+o exec --no-startup-id /home/synntix/.local/bin/oof
bindsym $sup+y exec --no-startup-id /home/synntix/.local/bin/oui
bindsym $sup+u exec --no-startup-id /home/synntix/.local/bin/non

###Layout binds
# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+b split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+z layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+q focus parent

# focus the child container
#bindsym $mod+d focus child

################ MISC ################ 

#Enabling floating by default de particuilar programs
for_window [window_role="pop-up"] floating enable
for_window [window_role="bubble"] floating enable
for_window [window_role="task_dialog"] floating enable
for_window [window_role="Preferences"] floating enable
for_window [window_type="dialog"] floating enable
for_window [window_type="menu"] floating enable
for_window [title="Open With"] floating enable
for_window [title="Authentification requise"] floating enable

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+ampersand workspace number $ws1
bindsym $mod+eacute workspace number $ws2
bindsym $mod+quotedbl workspace number $ws3
bindsym $mod+apostrophe workspace number $ws4
bindsym $mod+parenleft workspace number $ws5
bindsym $mod+minus workspace number $ws6
bindsym $mod+egrave workspace number $ws7
bindsym $mod+underscore workspace number $ws8
bindsym $mod+ccedilla workspace number $ws9
bindsym $mod+agrave workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+ampersand move container to workspace number $ws1
bindsym $mod+Shift+eacute move container to workspace number $ws2
bindsym $mod+Shift+quotedbl move container to workspace number $ws3
bindsym $mod+Shift+apostrophe move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+minus move container to workspace number $ws6
bindsym $mod+Shift+egrave move container to workspace number $ws7
bindsym $mod+Shift+underscore move container to workspace number $ws8
bindsym $mod+Shift+ccedilla move container to workspace number $ws9
bindsym $mod+Shift+agrave move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+c reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 2 px or 2 ppt
        bindsym Down resize grow height 2 px or 2 ppt
        bindsym Up resize shrink height 2 px or 2 ppt
        bindsym Right resize grow width 2 px or 2 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

